% !TEX root = twibop2.tex
\chapter*{Introduction}
\markboth{introduction}{introduction}
\label{ch-intro}

\epigraph{And chance, inventor God \ldots}{A. S. Pushkin}

\section*{A Discussion on the Role of Chance}


%{\setlength{\parindent}{0cm}
\begin{dialogue}
%\parindent{}
 \athr ``You wrote some nice words about chance in the Preface. In spite of them, I still think chance plays a negative role on the whole. Naturally, there is good luck, but everybody knows it is better not to count on it. Chance interferes with our plans, so it's better not hang on it, we should rather ward it off as much as possible.''

  \athr ``That is exactly the traditional attitude towards the random. However, it is an attitude we must clearly review. First of all, is it really possible to get by without the random?''

\rdr ``I don't say that it's possible. I said we should try.''

  \athr ``Suppose you work at an ambulance centre. Obviously, you cannot foresee when an ambulance will be needed, where it will be necessary to send it to, and how much time the patient will require. But a practical decision depends on all of these points. How many doctors should be on duty at anyone time? On the one hand, they should not be idle waiting for calls for long periods of time, yet on the other hand,  patients should not have to remain without aid for too long. You cannot avoid chance. What I am trying to say is: we cannot \redem{eliminate} chance, and so we must \redem{take} it \redem{into account}.''


\rdr ``True, we have to make peace with chance in this
  example. However, it still is a negative factor.''

\athr ``Thus, we see that sometimes we have to take
  chance into consideration rather than control it. But we can go
  further. We can discover situations in which chance becomes a
  positive factor rather than a negative one, so that it is desirable
  to raise the level of the random threshold.''

\rdr ``I don't understand you.''

\athr ``Of course, chance occasions interfere with our
  plans. At the same time because it makes us new solutions and
  improve our ability to create''.

\rdr ``Do you mean an improvement is obtained by
  overcoming difficulties?''

\athr  ``The main point is that randomness can create new
  possibilities. An American writer has written an interesting science
  fiction story. A group of scientists with various disciplines is officially
informed that a sensational discovery has been made, but unfortunately
the discoverer died in an explosion during a demonstration of the
phenomenon and thus the secret was lost. In reality neither the
invention nor the inventor ever existed. The scientists were presented
with the evidence of a tragedy: indistinct fragments of records, a
library, and an equipped laboratory. In other words, the scientists
were given a vast quantity of unconnected information with chance data
from various fields of science and technology. The evidence could be
called informational noise. The scientists were certain a discovery
had been made, and therefore the target was achievable. They utilized
all the information at their disposal and `revealed' the secret of the
non-existing invention. We might say that they succeeded in sifting information from the noise.''  

\rdr  ``But that's only a science fiction story.''  

\athr  ``True. However, the idea behind the story is far from being
fiction. Any discovery is related to the use of random factors.''  

\rdr  ``I don't think anyone can discover anything important
unless he or she has a professional grasp of the subject.''  

\athr  ``I think so too. Moreover, a discovery requires both
expertise on the part of the researcher and a certain level of the
development within the science as a whole. And yet \ldots, random factors
play a fundamental role in that.'' 

\rdr  ``As I understand, the word fundamental means something
primary, something at the basis. Can you apply the term fundamental to something random? I admit that randomness may be useful. But can it be fundamental? In the last analysis, we deal with random variables when there is something we do not know and cannot take into account. ''  

\athr  ``By believing that randomness is related to inadequate
knowledge, you make it \redem{subjective}. It follows that you believe that
randomness appears, as it were, on the surface and that there is
nothing random at the basis of phenomena. Is it correct?''  

\rdr  ``Precisely. That is why we cannot assert randomness is
fundamentality. As science develops, our ability to take different
factors into account increases, and the result is that the domain of
random variables will gradually recede. There is sense in saying that
 science is the enemy of chance. ''  

\athr ``You're not quite right. Indeed, the advance of
  science enhances our ability to make scientific predictions, that
  is, science is against the random factor. But at the same time, it
  turns out that while our scientific knowledge becomes deeper, or,
  more accurately, while we look at the molecular and atomic aspects
  of phenomena, randomness not only does not become less important,
  bit on the contrary, it reigns supreme. Its existence proves to be
  independent of the degree of our knowledge. Randomness reveals its
  \redem{fundametality} at the level of the microcosm.''

\rdr  ``This is the first time I've heard someone say
  that. Please tell me more.''

\athr  ``Let me say at once that this topic has had a long
  history. It was first formalized in Ancient Greece with two
  approaches to the random being stated. The two views are associated
  with the names of Democritus and Epicurus. Democritus identified the
  random with the \redem{unknown}, believing that Nature is completely
  deterministic. He said:  People have created an idol out of the
    random as a cover for their inability to think things out.
  Epicurus considered that the random is inherent in various
  phenomena, and that it is, therefore, \redem{objective}. Democritus's
  point of view was preferred for a long time, but in the $20^{\text{th}}$ century, the progress of science showed that Epicurus was right. In his doctoral thesis \redem{Difference Between the Democritian and Epicurian Philosophy on Nature} (1841), Karl Marx positively evaluated Epicurus's view of the random and pointed out the deep philosophical significance of the teachings of Epicurus on the  \redem{spontaneous}  displacement of atoms. Of course, we should not exaggerate the contribution of Epicurus to our understanding of the random because he could only guess.''

\rdr  ``It turns out that I presented Democritus's views
  on the random without knowing it. But I would like to have some
  concrete examples showing the fundamentality of the random.''

\athr  ``Consider, for instance, a nuclear-powered
  submarine. How is the engine started?'' 

\rdr  ``As far as I understand it, special
  neutron-absorbing rods are drawn from the core of the reactor. Then
  a controlled chain reaction involving the fission of uranium nuclei
  begins \ldots{}'' 

\athr ``(interrupting)  Let us try and see how everything begins.''

\rdr  ``After entering a uranium nucleus, a neutron triggers its
disintegration into two fragments and another neutron is released. The
neutrons split two more uranium nuclei; four neutrons are then set
free, which in turn split four more nuclei. The process develops like an avalanche.''

\athr  ``All right. But where does the first neutron come from?''

\rdr  ``Who knows? Say, they come from cosmic rays.''

\athr  ``The submarine is deep under water. The thick
  layer of water protects it from cosmic rays.''

\rdr  ``Well then, I don't know \ldots{}''

\athr   ``The fact is that a uranium nucleus may either
  split because a neutron enters it or it may decay
  \redem{spontaneously. The process of spontaneous nuclear fission is
    random.}''

\rdr ``But maybe spontaneous nuclear fission is caused
  by factors we do not know about yet.''

\athr  ``This is something physicists have been trying to
  solve. Many attempts have been made to find the  hidden
    parameters which govern the processes in the microcosm. It has
  been concluded that there are no such parameters, and therefore
  randomness in the microcosm is fundamental. This cornerstone problem
  is thoroughly treated in \redem{quantum mechanics}, a theory which
  appeared in the early $20^{\text{th}}$ century in connection with research
  on atomic processes.''

\rdr  ``The only thing I know about quantum mechanics is
  that it describes the laws governing the behaviour of elementary particles.''
  
\athr  ``We shall talk about quantum mechanics in more
  detail later. Let me only note here that it demonstrates the
  fundamental role of spontaneous processes and, therefore,
  demonstrates the fundamentality of the random. The operation of any
  radiation generator, from a vacuum tube to a laser, would be
  impossible without spontaneous processes. They are fundamental as
  the  trigger without which the radiation generation would not start.''

\rdr  ``And yet, it is difficult for me to believe that
  randomness is fundamental. You mentioned a nuclear-powered
  submarine. When the captain orders that the engines be turned on, he
  does not rely on a lucky chance. An appropriate button is pressed,
  and the engines start (if they are in good condition). The same can
  be said when a vacuum tube is turned on. Where is the randomness here?''

\athr  ``Nevertheless, when we consider phenomena in the
  microcosm, the processes are triggered by random factors.'' 

\rdr  ``However, we generally deal with processes
  occurring in the macrocosm.''

\athr  ``Firstly, while studying the world around us and
  trying to comprehend its \redem{cause and effect relations}, we must
  address the atomic level, i. e., the level of microcosm
  phenomena. Secondly, the randomness in microcosmic phenomena is
  essentially reflected in the processes observed at the macrocosmic scale.''
  
\rdr  ``Can you give me an example when the
  fundamentality of randomness reveals itself at the macrocosmic scale?''
  
\athr ``\redem{Evolution}, which is a continuous process in both
  the plant and animal kingdoms, may serve as an example. Evolution
  relies on mutation, i.e., random changes in the structure of
  genes. A random mutation may be rapidly spread by the reproduction
  of the organisms themselves. It is essential that selection occurs
  simultaneously with mutation. The organisms which contain the random
  gene are then selected to that those best fitted to their
  environment survive. In consequence, evolution requires the
  \redem{selection of random gene changes}.''

\rdr  ``I don't quite understand this business of
  selection.''
  
\athr ``Here's an example. The flowers of a certain
  orchid look like a female wasp. They are pollinated by male wasps
  which take the flowers to be females. Suppose a mutation occurs, and
  the shape and colour of the flower are changed. The flower will then
  remain unpollinated. The result is that the mutation is not passed
  on to the new generation. It may be said that selection rejected the
  mutation which changed the outward appearance of the flower. There
  was a species of orchid which became a self-pollinator, the flowers
  of this  species rapidly acquired diverse shape and colour owing to
  the mutation.''

\rdr  ``As far as I know, evolution progresses in the
  direction of the differentiation of species. Doesn't this show that
  the mutations underlying evolution are not, in fact, so random?''

\athr  ``That argument doesn't stand to reason. Evolution
  selects the fittest organisms rather than the more
  complex. Sometimes a higher degree of organization is preferable,
  but sometimes this is not the case. This is why human beings,
  jelly-fish, and the influenza virus can coexist in today's world. It
  is essential that evolution leads to the appearance of new species
  that are unpredictable in principle. It may be said that \redem{any species is unique because it occurred fundamentally} by chance.''

\rdr  ``I have to admit that the randomness does look to be
a fundamental factor indeed.''

\athr  ``Since we are discussing the fundamentality of
  randomness in the picture of evolution, let me draw your attention
  to one more important point. Modern science demonstrates that
  \redem{chance} and \redem{selection} are the `creator'.''

\rdr  ``Just as Pushkin said, `And chance, inventor God \ldots{}'''

\athr  ``Precisely. This line is strikingly accurate.''

\rdr  ``It appears that when speaking about chance and
  selection, we should imply the \redem{selection of information from noise}, shouldn't we? The same selection that we discussed in connection
  with the science-fiction story.''
  
\athr `` Absolutely.''

\rdr  ``I have to agree that we should consciously
  recognize the existence of randomness rather than try and control it.''

\athr  ``We could say more. Naturally, the randomness
  which is due to the incompleteness of our knowledge is
  undesirable. While studying the world, man has fought, is fighting,
  and will continue to fight it. It should be noted at the same time
  that there is an \redem{objective} randomness underlying every
  phenomena along with the \redem{subjective} randomness which is due
  to lack of data on a phenomenon. We should also take into account
  the positive, creative role of the random. And in this connection it
  is really necessary to recognize and control randomness. Man should
  be able, when necessary, to create special situations, abundant with
  the random, and utilize the situation to his own ends.''

\rdr  ``But is it really possible to treat randomness in
  such a way? Isn't it like trying \redem{to control the uncontrollable}?''

\athr   ``Both science and daily life indicate that it is
  possible to orient ourselves consciously in very random
  situations. Special calculation methods have been developed that
  depend on randomness. Special theories have been produced, such as
  \redem{queueing theory}, the \redem{theory of games}, and the
  \redem{theory of random search}, to deal with it.''

\rdr  ``It is hard for me to imagine a scientific theory
  built on randomness.''

\athr  ``Let me emphasize right away that randomness does
  not preclude scientific prediction. The fundamentality of randomness
  does not mean that the world around us is chaotic and devoid of
  order. Randomness does not imply there are no causal relations. But
  we shall deal with all that later. It is interesting to try and
  imagine a world in which randomness as an objective factor is
  \redem{completely absent}.''

\rdr  ``This would be an ideally ordered world.''

\athr ``In such a world, the state of any object at a
  given time would be unambiguously determined by its past states and,
  in its turn, would determine the future states just as
  definitely. The past would be strictly connected with the present,
  as would the present with the future.''

\rdr  ``Anything occurring in such a world would be
predetermined.''

\athr  ``Pierre Laplace, a great French scientist of the
  $17^{\text{th}}$ century, suggested in this connection that we imagine a superbeing who knew the past and the future of such a world in
  every detail. Laplace wrote:''  
  \begin{quote}
  `The intellect who could know, at a
    given moment, every force that animates the nature and the
    relative positions of its every component, and would, in addition,
    be vast enough to analyse these data, would describe by a single
    formula the motions of the greatest bodies in the universe and the
    motions of the lightest atoms. There would be nothing uncertain
    for this being, and the future, like the past, be open to his gaze.'
\end{quote}

\rdr  ``An ideally ordered world is therefore unreal.''

\athr  ``As you see, it isn't hard to feel that the real
  world should admit the existence of objective randomness. Now let us
  return to the problem of causal relations. These relations are
  \redem{probabilistic} in the real world. It is only in particular cases (for
  example, when solving maths problems at school) that we deal with
  unambiguous, strictly determined relations. Here we approach one of
  the most essential notions of the modern science, the notion of
  \redem{probability}.''

\rdr  ``I'm familiar with it. If I throw a die, I can
  equally expect any number of dots from one to six. The probability
  of each number is the same and equal to 1/6.''

\athr  ``Suppose you stand at the side of a road, with
  motor-cars passing by. What is the probability of the first two
  digits in their four digit number being equal?''

\rdr  ``The probability equals 1/10.''

\athr  ``Therefore, if you're patient and observe enough
  cars, about one tenth of them will have number-plates with the same
  first two digits, would they? Say, about thirty cars out of 300 will
  have such plates. Maybe, 27 or 32, but not 10 or 100.''

\rdr  ``I think so.''

\athr  ``But then there would be no need to stand at the roadside.
The result could be predicted. This is an example of \redem{probabilistic prediction}. Look at how many random factors are involved in this situation. A car could turn off the road before reaching the observer,
or another car could stop or even turn back. And nonetheless, both
today and tomorrow, about 30 cars out of 300 would have plates with
the same first two digits.''

\rdr ``So, in spite of numerous random factors, the
  situation has a certain constancy.''
  
\athr  ``This constancy is commonly called \redem{statistical
  stability}. It is essential that statistical stability is observed
  because of random factors rather than despite them.''

\rdr  ``I hadn't thought that we deal with probabilistic
  predictions everywhere. They include, for instance, sports
  predictions and weather forecasts.''

\athr  ``You're absolutely right. An important point is
  that probabilistic (statistical) causal relations are common, while
  those leading to unambiguous predictions are just a special
  case. While definite predictions only presuppose the necessity of a
  phenomenon, probabilistic predictions are related simultaneously
  both with necessity and randomness. Thus, mutations are random, but
  the process of selection is governed by laws, that is, it is a
  necessary prerequisite.''

\rdr  ``I see. The individual acts of the spontaneous
  fission of uranium nuclei are random, but the development of the
  chain reaction is unavoidable.''

\athr  ``Taken separately, any discovery is
  random. However, a situation which is favourable for the appearance
  of such a chance should exist. This chance is determined by the
  advance of science, the expertise of the researchers, and the level
  of measurement technology. A discovery is random, but the logic of
  the progress leading to the discovery in the long run is regular,
  unavoidable, and necessary.''

\rdr  ``Now I see why the fundamentality of randomness does not
result in the disorder of our world. Randomness and necessity are
always combined.''

\athr   ``Correct. Friedrich Engels wrote in \redem{The
    Origin of the Family, Private Property, and the State} (1884):
   `In Nature, where chance also seems to reign, we have long ago
    demonstrated in each particular field the inherent necessity and
    regularity that asserts itself in this chance.' The Hungarian
    mathematician A.R\'enyi wrote about the same thing in an
    interesting book \redem{Letters on Probability}'':  
    \begin{quote}
    `I came across \redem{Contemplations} by
    Aurelius and accidentally opened the page where he wrote about two
    possibilities: the world is either in vast chaos or, otherwise,
    order and regularity reign supreme. And although I had read these
    lines many times, it was the first time that I thought over why
    Marcus Aurelius believed that the world should be dominated by
    either chance or order. Why did he believe that these two
    possibilities are contradictory? The world is dominated by
    randomness, but order and regularity operate at the same time,
    being shaped out of the mass of random events according to the
    laws of the random.'
	\end{quote}
\rdr  ``As far as I understand, order and regularity are
  produced from a mass of random events, and this leads to the concept
  probability.
''
\athr  ``You're absolutely right. \redem{Individual factors} vary
  from case to case. At the same time, the \redem{picture as a whole} remains
  stable. This stability is expressed in terms of \redem{probability}. This is
  why our world proves to be flexible, dynamic, and capable of advancing.''

\rdr  ``It follows that the world around us may justly be
  said to be a world of probability.''

\athr  ``It is better to speak of the \redem{world as being
    built on probability}. When we examine this world, we shall
  concentrate on two groups of questions. Firstly, I shall show how
  man, owing to his use of probability in science and technology, was
  able to  tame randomness and thus turn it from being his enemy
  into an ally and friend. Secondly, using the achievements of modern
  physics and biology, I shall demonstrate the probabilistic features
  of the laws of nature. In consequence, I shall show that the world
  around us (including both the natural and artificial world) is
  really built on probability.''
  
\end{dialogue}
\cleardoublepage

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "tarasov-twibop"
%%% End:
